# Ejercicios P2P

Serie de ejercicios para el curso de Desarrollo Frontend con HTML, CSS y Javascript (10ª edición) de Miriadax.

Ejercicios accesibles en:
* [Módulo 1. Introducción a HTML y CSS, estructura, herencia, ...](https://jtr0.neocities.org/Ejercicio1.html)
* [Módulo 2. Menus, Estructura CSS, Texto, Fuentes, Imágenes y Multimedia](https://jtr0.neocities.org/mod2/index.html)
* [Módulo 3. Introducción a ES6+, let, VS Code, DOM, Boolean, if, bucles,...](https://jtr0.neocities.org/mod3/index.html)
* [Módulo 4. Eventos, Objetos, Arrays, MVC, JSON, Boolean y Storage API](https://jtr0.neocities.org/mod4/index.html)
* [Módulo 5. Tipos y clases, herencia, Number, multi-asignación, Spread y Rest ](https://jtr0.neocities.org/mod5/index.html)
* [Módulo 6. Strings, iteradores y bucles, programación funcional y Regexp](https://jtr0.neocities.org/mod6/index.html)

Este curso (https://miriadax.net/web/html5mooc) cubre el desarrollo de aplicaciones front-end para terminales fijos y móviles (PC, teléfono móvil, tableta, ...) en  HTML, CSS y JavaScript, tecnologías con las que se diseñan hoy día las aplicaciones de cliente en Internet.

El curso comienza con los fundamentos de HTML, CSS y JavaScript para facilitar el aprendizaje a principiantes. Continúa con temas más avanzados de frontend, como MVC de cliente, clases, diseño de juegos, procesado de strings, RegExps, excepciones, promesas, AJAX, UX, accesibilidad, gestures, SVG, animaciones o PWAs. Se hace especial hincapié en ilustrar las últimas mejoras de HTML, CSS y JavaScript. Se utiliza un enfoque integral y multidisciplinar que cubre los lenguajes, herramientas recomendadas, metodologías de programación, diseño gráfico adaptativo, diseño de experiencia de usuario, SEO, etc. Todo está ilustrado con ejemplos sencillos, pero realistas en el marco de la metodología AMMIL que facilita el auto-aprendizaje.

Se enseña también a empaquetar las apps del navegador a un teléfono móvil (Android o iOS), tableta o escritorio como PWAs (Progressive Web Apps).
